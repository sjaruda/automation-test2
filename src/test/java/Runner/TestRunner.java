package Runner;


import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "src/test/java/Feature", glue = "Steps", tags = {"@excel" })

public class TestRunner extends AbstractTestNGCucumberTests {
}
