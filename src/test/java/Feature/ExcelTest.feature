@excel

    Feature:  Excel test data

    Scenario Outline: Data driven with excel and data sets
    When I open status page
    Then I add the data from excel "<row_index>" dataset
    Examples:
    |row_index|
    |2        |
    |3        |
    |4        |

