@login
Feature: Login feature

  Scenario: Login with correct username and password
    Given I navigate to login page
    And I enter the following for login
    | username | password|
    | S1234    | test1   |
    When I click login button
    Then I should see the status page




