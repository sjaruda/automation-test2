@status
Feature: Post status feature

  Scenario Outline: Post a status
    Given I navigate to the Status Post System page
    When I enter the "<code>" and "<status>"
    And I click the post button
    Then I should navigate to my status

    Examples:
      | code      | status  |
      | S1111     | test1   |
      | S2222     | test2   |
