package Steps;

import Pages.PostStatusPage;
import Utils.DataHelper;
import java.util.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ExcelTestSteps {

    public WebDriver driver;
    public List<HashMap<String,String>> datamap;

    public ExcelTestSteps()
    {
        driver = Hook.driver;
        datamap = DataHelper.data("C:\\Sherryl\\SourceTree\\4_PostStatus\\src\\test\\java\\testdata1.xlsx","Sheet1");
    }

    @When("^I open status page$")
    public void i_open_automationpractice_website() throws Throwable {

      // driver.navigate().to("http://localhost:8080/portfolio/diarySystem_Synchronous_WebDev/poststatusform.php");
    }

    @Then("^I add the data from excel \"(.*?)\" dataset$")
    public void i_contact_the_customer_service_with_excel_row_dataset(String arg1) throws Throwable {
        int index = Integer.parseInt(arg1)-1;

        PostStatusPage statusPage = new PostStatusPage(driver);


        for(HashMap h:datamap)
        {

            String code = (String) h.values().toArray()[0];
            String status = (String) h.values().toArray()[1];


            //System.out.println("this is the keyset VVVVVVVV:" + h.keySet());

            //System.out.println("this is the value SSSSSSSSSSSS:" + h.values());

            //System.out.println("Value1: " + code);

           //System.out.println("Value2: " + status);

        }

        driver.findElement(By.name("statuscode")).sendKeys(datamap.get(index).get("code"));
        driver.findElement(By.name("status")).sendKeys(datamap.get(index).get("status"));
        statusPage.clickPost();
    }
}


