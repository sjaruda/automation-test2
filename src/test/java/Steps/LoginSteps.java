package Steps;
import Base.TestBase;

import Pages.LoginPage;
import cucumber.api.DataTable;
import java.util.ArrayList;
import java.util.List;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.testng.Assert;


public class LoginSteps extends TestBase{

    private TestBase testBase;

    public LoginSteps(TestBase testBase){
        this.testBase = testBase;
    }



    @Given("^I navigate to login page$")
    public void iNavigateToLoginPage() throws Throwable {

        System.out.println("I navigate to the login page");

       // testBase.Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
       //testBase.Driver.navigate().to("http://localhost:8080/portfolio/diarySystem_Synchronous_WebDev/poststatusform.php");
    }

    @And("^I enter the following for login$")
    public void iEnterTheFollowingForLogin(DataTable table) throws Throwable {

        List<User> users = new ArrayList<User>();
        users = table.asList(User.class);

        LoginPage page = new LoginPage(testBase.Driver);

        for (User user: users){
            page.EnterLoginDetails(user.username, user.password);
        }

    }

    @When("^I click login button$")
    public void iClickLoginButton() throws Throwable {

        LoginPage statusPostingPage = new LoginPage(testBase.Driver);

        statusPostingPage.ClickPost();
    }

    @Then("^I should see the status page$")
    public void iShouldSeeTheStatusPage() throws Throwable {

        Assert.assertEquals(testBase.Driver.findElement(By.name("yourstatus")).isDisplayed(), true, "Its not displayed");
    }


    public class User{

        public String username;
        public String password;

        public User(String userName, String passWord){
            username = userName;
            password = passWord;
        }
    }

}
