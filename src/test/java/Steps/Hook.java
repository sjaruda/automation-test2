package Steps;

import Base.TestBase;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class Hook extends TestBase {

    public static WebDriver driver;

    public Hook(){

    }


    @Before
    public void InitializeTest()
    {

        System.out.println("START BROWSER HOOK- SHERRYL");

        System.setProperty("webdriver.chrome.driver", "C:\\Sherryl\\AutomationTesting\\Drivers\\chromedriver.exe");
        driver = new ChromeDriver();

       // testBase.Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.navigate().to("http://localhost:8080/portfolio/diarySystem_Synchronous_WebDev/poststatusform.php");
    }

    @After
    public void ClosingTheBrowserTest(Scenario scenario)
    {
        if(scenario.isFailed()){
            //take screenshots
            System.out.println(scenario.getName());
        }

        System.out.println("CLOSE BROWSER HOOK- SHERRYL");
        //testBase.close();
        driver.close();

    }
}
