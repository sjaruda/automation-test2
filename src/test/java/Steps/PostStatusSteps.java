package Steps;

import Base.TestBase;
import Pages.PostStatusPage;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.testng.Assert;

public class PostStatusSteps extends TestBase {

    TestBase testBase;

    public  PostStatusSteps(TestBase testBase){

        this.testBase = testBase;
    }



    @Given("^I navigate to the Status Post System page$")
    public void iNavigateToTheStatusPostSystemPage() throws Throwable {

        String pageTitle = testBase.Driver.findElement(By.xpath("//*/body/h1")).getText();
        Assert.assertEquals(pageTitle, "Status Posting System");
    }


    @When("^I enter the \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_enter_the_and(String code, String status) throws Throwable {

        PostStatusPage statusPage = new PostStatusPage(testBase.Driver);
        statusPage.addCodeAndStatus(code, status);
    }

    @And("^I click the post button$")
    public void iClickThePostButton() throws Throwable {
        PostStatusPage statusPage = new PostStatusPage(testBase.Driver);
        statusPage.clickPost();
    }

    @Then("^I should navigate to my status$")
    public void iShouldNavigateToMyStatus() throws Throwable {
        String yourStatusTitle = testBase.Driver.findElement(By.xpath("/html/body/h1")).getText();
        Assert.assertEquals(yourStatusTitle, "Your Status");
    }
}
