package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PostStatusPage {

    public PostStatusPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    //finding the correct page, displaying correct page name
    @FindBy(how = How.XPATH, using = "//*/body/h1")
    public WebElement statusPage;

    //finding code and status box in the page
    @FindBy(how = How.ID, using = "statuscode")
    public WebElement codeBox;

    @FindBy(how = How.NAME, using = "status")
    public WebElement statusBox;

    //keying value inside the box
    public void addCodeAndStatus(String code, String status) {
        codeBox.sendKeys(code);
        statusBox.sendKeys(status);
    }

    //finding the postButton
    @FindBy(how = How.NAME, using = "post")
    public WebElement postButton;

    public void clickPost(){
        postButton.click();
    }
}





