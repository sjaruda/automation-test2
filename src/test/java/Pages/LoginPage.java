package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    public LoginPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.NAME,using = "statuscode")
    public WebElement txtUserName;

    @FindBy(how = How.NAME,using = "status")
    public WebElement txtPassWord;

    @FindBy(how = How.NAME,using = "post")
    public WebElement btnLogin;

    public void EnterLoginDetails(String userName,String passWord){

        txtUserName.sendKeys(userName);
        txtPassWord.sendKeys(passWord);
    }
 
    public void ClickPost(){
        btnLogin.click();
    }
}
